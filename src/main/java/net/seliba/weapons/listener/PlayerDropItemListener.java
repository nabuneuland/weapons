package net.seliba.weapons.listener;

import net.seliba.weapons.Weapons;
import net.seliba.weapons.weapons.Weapon;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

public class PlayerDropItemListener implements Listener {

    @EventHandler
    public void onDrop(PlayerDropItemEvent event) {
        Player player = event.getPlayer();
        for(Weapon weapon : Weapons.getWeaponList()) {
            if(weapon.getItemStack() == event.getItemDrop().getItemStack()) {
                player.sendMessage("§6CTF §7» §aDeine Waffe lädt nach...");
                Bukkit.getScheduler().runTaskLaterAsynchronously(Weapons.getInstance(),
                        () -> {
                            weapon.reload();
                            player.sendMessage("§6CTF §7» §aDeine Waffe hat erfolgreich nachgeladen!");
                        },
                        60L
                );
            }
        }
    }

}

package net.seliba.weapons.listener;

import net.seliba.weapons.weapons.Weapon;
import net.seliba.weapons.weapons.WeaponType;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.HashMap;
import java.util.Map;

public class ProjectileHitListener implements Listener {

    private static Map<Entity, WeaponType> projectileList = new HashMap<>();
    private static Map<Entity, Location> projectileDistance = new HashMap<>();

    @EventHandler
    public void onProjectileHit(ProjectileHitEvent event) {
        Entity projectile = event.getEntity();

        if(projectile.getType() != EntityType.SNOWBALL) {
            return;
        }

        if(!(event.getEntity().getShooter() instanceof CraftPlayer)) {
            return;
        }

        if(event.getHitEntity() == null) {
            return;
        }

        if(projectileList.get(projectile) == WeaponType.RIFLE) {
            System.out.println(5);
            ((LivingEntity) event.getHitEntity()).damage(WeaponType.RIFLE.getDamage());
        } else if(projectileList.get(projectile) == WeaponType.SHOTGUN) {
            double distance = projectileDistance.get(projectile).distance(event.getHitEntity().getLocation());
            if(distance <= 5) {
                ((LivingEntity) event.getHitEntity()).damage(WeaponType.SHOTGUN.getDamage());
            } else if(distance <= 10) {
                ((LivingEntity) event.getHitEntity()).damage(WeaponType.SHOTGUN.getDamage() / 1.5);
            } else if(distance <= 20) {
                ((LivingEntity) event.getHitEntity()).damage(WeaponType.SHOTGUN.getDamage() / 3);
            }
        }
    }

    public static void registerProjectile(Entity projectile, WeaponType weaponType, Location shotLocation) {
        projectileList.put(projectile, weaponType);
        projectileDistance.put(projectile, shotLocation);
    }

}

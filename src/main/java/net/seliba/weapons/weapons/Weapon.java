package net.seliba.weapons.weapons;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public abstract class Weapon {

    protected final int MAX_AMMO = 20;

    protected int ammo;
    protected long delay;
    protected double width;
    protected ItemStack itemStack;

    public Weapon(long delay, double width, ItemStack itemStack) {
        this.ammo = MAX_AMMO;
        this.delay = delay;
        this.width = width;
        this.itemStack = itemStack;
        ItemMeta meta = itemStack.getItemMeta();
        meta.setDisplayName("§a" + getName() + " §7| §a" + ammo);
        itemStack.setItemMeta(meta);
    }

    public int getAmmo() {
        return ammo;
    }

    public void setAmmo(int ammo) {
        this.ammo = ammo;
    }

    public long getDelay() {
        return delay;
    }

    public void setDelay(long delay) {
        this.delay = delay;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    public void setItemStack(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

    public void reload() {
        setAmmo(MAX_AMMO);
    }

    public void empty() {
        setAmmo(0);
    }

    public abstract void fire(Player player);

    public abstract String getName();

    protected String translate(String string) {
        return ChatColor.translateAlternateColorCodes('&', string).replaceAll("§", "\u00A7");
    }
}

package net.seliba.weapons.weapons;

import org.bukkit.Material;

public enum WeaponType {

    RIFLE(Material.DIAMOND_HOE, 4),
    SHOTGUN(Material.STONE_HOE, 12);

    private Material material;
    private int damage;

    WeaponType(Material material, int damage) {
        this.material = material;
        this.damage = damage;
    }

    public Material getMaterial() {
        return material;
    }

    public int getDamage() {
        return damage;
    }

}

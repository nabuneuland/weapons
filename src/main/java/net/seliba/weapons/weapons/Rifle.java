package net.seliba.weapons.weapons;

import net.seliba.weapons.listener.ProjectileHitListener;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Rifle extends Weapon {

    public Rifle() {
        super(300, 500, new ItemStack(WeaponType.RIFLE.getMaterial()));
    }

    @Override
    public void fire(Player player) {
        ammo--;

        ItemStack itemStack = getItemStack();
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName("§aSturmgewehr §8| §a" + (ammo >= 0 ? ammo : 0));
        itemStack.setItemMeta(itemMeta);
        player.getInventory().setItemInMainHand(itemStack);

        if (ammo <= -1) {
            player.sendMessage("§6CTF §7» §aDu hast deine Munition aufgebraucht!");
            return;
        }
        Snowball snowball = player.launchProjectile(Snowball.class);
        snowball.setVelocity(player.getLocation().getDirection().multiply(width / 100));
        ProjectileHitListener.registerProjectile(snowball, WeaponType.RIFLE, player.getLocation());
    }

    @Override
    public String getName() {
        return translate("Sturmgewehr");
    }
}

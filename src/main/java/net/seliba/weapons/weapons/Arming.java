package net.seliba.weapons.weapons;

import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Arming {

    private static Map<UUID, Arming> players = new HashMap<>();

    public static Arming getPlayer(UUID uuid) {
        if (!players.keySet().contains(uuid))
            players.put(uuid, new Arming());

        return players.get(uuid);
    }

    public static Arming getPlayer(Player player) {
        return getPlayer(player.getUniqueId());
    }

    private Rifle rifle;
    private Shotgun shotgun;

    public Rifle getRifle() {
        return rifle;
    }

    public void setRifle(Rifle rifle) {
        this.rifle = rifle;
    }

    public Shotgun getShotgun() {
        return shotgun;
    }

    public void setShotgun(Shotgun shotgun) {
        this.shotgun = shotgun;
    }
}

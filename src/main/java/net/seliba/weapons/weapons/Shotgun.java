package net.seliba.weapons.weapons;

import net.seliba.weapons.listener.ProjectileHitListener;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

public class Shotgun extends Weapon {

    public Shotgun() {
        super(1000, 200, new ItemStack(WeaponType.SHOTGUN.getMaterial()));
    }

    @Override
    public void fire(Player player) {
        ammo--;

        if (ammo <= -1) {
            player.sendMessage("§6CTF §7» §aDu hast deine Munition aufgebraucht!");
            return;
        }

        ItemStack itemStack = getItemStack();
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName("§aSchrotflinte §8| §a" + (ammo >= 0 ? ammo : 0));
        itemStack.setItemMeta(itemMeta);
        player.getInventory().setItemInMainHand(itemStack);
        
        for(double i = -0.2; i < 0.3; i += 0.1) {
            Snowball snowball = player.launchProjectile(Snowball.class);
            snowball.setVelocity(player.getLocation().getDirection().add(new Vector(i, 0, 0)).multiply(width / 100));
            ProjectileHitListener.registerProjectile(snowball, WeaponType.SHOTGUN, player.getLocation());
        }
    }

    @Override
    public String getName() {
        return translate("Schrotflinte");
    }
}

package net.seliba.weapons;

import net.seliba.weapons.listener.PlayerDropItemListener;
import net.seliba.weapons.listener.ProjectileHitListener;
import net.seliba.weapons.weapons.*;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public class Weapons extends JavaPlugin implements Listener {

    private static List<Weapon> weaponList = new ArrayList<>();
    private static Weapons instance;

    @Override
    public void onEnable() {
        instance = this;
        Bukkit.getPluginManager().registerEvents(this, this);
        Bukkit.getPluginManager().registerEvents(new ProjectileHitListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerDropItemListener(), this);
    }

    @EventHandler
    public void onPlayerInteractEvent(PlayerInteractEvent event) {
        if (event.getItem() == null)
            return;

        if (event.getItem().getType().equals(WeaponType.RIFLE.getMaterial())) {
            Rifle rifle = Arming.getPlayer(event.getPlayer()).getRifle();
            if (rifle == null)
                return;

            rifle.fire(event.getPlayer());
        } else if (event.getItem().getType().equals(WeaponType.SHOTGUN.getMaterial())) {
            Shotgun shotgun = Arming.getPlayer(event.getPlayer()).getShotgun();
            if (shotgun == null)
                return;

            shotgun.fire(event.getPlayer());
        }
    }

    public static List<Weapon> getWeaponList() {
        return weaponList;
    }

    public static Weapons getInstance() {
        //Keiner mag Singletons
        return instance;
    }

}
